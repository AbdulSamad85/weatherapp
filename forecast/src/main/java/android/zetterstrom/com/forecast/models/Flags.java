/*
 * Copyright 2016 Kevin Zetterstrom
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package android.zetterstrom.com.forecast.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.Nullable;
import android.zetterstrom.com.forecast.typeconverts.StringListConverter;
import android.zetterstrom.com.forecast.typeconverts.UnitTypeConverter;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * The flags object contains various metadata information related to the request.
 * <p/>
 * Not all fields may contain data. A null field is indicative that it was not present in the response.
 * <p/>
 * <p/>
 * Created by Kevin Zetterstrom on 2/10/16.
 */
@SuppressWarnings("unused")
@Entity
public class Flags implements Serializable {

    private static final long serialVersionUID = 2335711403921076215L;

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = ModelConstants.FLAGS_ID)
    private int id;

    @Nullable
    @SerializedName(ModelConstants.FIELD_DARKSKY_UNAVAILABLE)
    private String darkSkyUnavailable;

    @Nullable
    @SerializedName(ModelConstants.FIELD_DARKSKY_STATIONS)
    @ColumnInfo(name = ModelConstants.FIELD_DARKSKY_STATIONS)
    @TypeConverters(StringListConverter.class)
    private List<String> darkSkyStations;

    @Nullable
    @SerializedName(ModelConstants.FIELD_DATAPOINT_STATIONS)
    @ColumnInfo(name = ModelConstants.FIELD_DATAPOINT_STATIONS)
    @TypeConverters(StringListConverter.class)
    private List<String> dataPointStations;

    @Nullable
    @SerializedName(ModelConstants.FIELD_ISD_STATIONS)
    @ColumnInfo(name = ModelConstants.FIELD_ISD_STATIONS)
    @TypeConverters(StringListConverter.class)
    private List<String> ISDStations;

    @Nullable
    @SerializedName(ModelConstants.FIELD_LAMP_STATIONS)
    @ColumnInfo(name = ModelConstants.FIELD_LAMP_STATIONS)
    @TypeConverters(StringListConverter.class)
    private List<String> lampStations;

    @Nullable
    @SerializedName(ModelConstants.FIELD_MADIS_STATIONS)
    @ColumnInfo(name = ModelConstants.FIELD_MADIS_STATIONS)
    @TypeConverters(StringListConverter.class)
    private List<String> madisStations;

    @Nullable
    @SerializedName(ModelConstants.FIELD_METAR_STATIONS)
    @ColumnInfo(name = ModelConstants.FIELD_METAR_STATIONS)
    @TypeConverters(StringListConverter.class)
    private List<String> metarStations;

    @Nullable
    @SerializedName(ModelConstants.FIELD_METNO_LICENSE)
    @ColumnInfo(name = ModelConstants.FIELD_METNO_LICENSE)
    private String metnoLicense;

    @Nullable
    @SerializedName(ModelConstants.FIELD_SOURCES)
    @ColumnInfo(name = ModelConstants.FIELD_SOURCES)
    @TypeConverters(StringListConverter.class)
    private List<String> sources;

    @Nullable
    @SerializedName(ModelConstants.FIELD_UNITS)
    @ColumnInfo(name = ModelConstants.FIELD_UNITS)
    @TypeConverters(UnitTypeConverter.class)
    private Unit unit;

    public Flags() {
    }

    @Nullable
    public List<String> getDarkSkyStations() {
        return darkSkyStations;
    }

    @Nullable
    public List<String> getDataPointStations() {
        return dataPointStations;
    }

    @Nullable
    public List<String> getISDStations() {
        return ISDStations;
    }

    @Nullable
    public List<String> getLampStations() {
        return lampStations;
    }

    @Nullable
    public List<String> getMadisStations() {
        return madisStations;
    }

    @Nullable
    public List<String> getMetarStations() {
        return metarStations;
    }

    @Nullable
    public String getMetnoLicense() {
        return metnoLicense;
    }

    @Nullable
    public List<String> getSources() {
        return sources;
    }

    @Nullable
    public Unit getUnit() {
        return unit;
    }

    @Nullable
    public String getDarkSkyUnavailable() {
        return darkSkyUnavailable;
    }

    public void setDarkSkyUnavailable(@Nullable String darkSkyUnavailable) {
        this.darkSkyUnavailable = darkSkyUnavailable;
    }

    public void setDarkSkyStations(@Nullable List<String> darkSkyStations) {
        this.darkSkyStations = darkSkyStations;
    }

    public void setDataPointStations(@Nullable List<String> dataPointStations) {
        this.dataPointStations = dataPointStations;
    }

    public void setISDStations(@Nullable List<String> ISDStations) {
        this.ISDStations = ISDStations;
    }

    public void setLampStations(@Nullable List<String> lampStations) {
        this.lampStations = lampStations;
    }

    public void setMadisStations(@Nullable List<String> madisStations) {
        this.madisStations = madisStations;
    }

    public void setMetarStations(@Nullable List<String> metarStations) {
        this.metarStations = metarStations;
    }

    public void setMetnoLicense(@Nullable String metnoLicense) {
        this.metnoLicense = metnoLicense;
    }

    public void setSources(@Nullable List<String> sources) {
        this.sources = sources;
    }

    public void setUnit(@Nullable Unit unit) {
        this.unit = unit;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


}
