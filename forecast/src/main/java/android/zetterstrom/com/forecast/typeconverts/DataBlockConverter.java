package android.zetterstrom.com.forecast.typeconverts;

import android.arch.persistence.room.TypeConverter;
import android.zetterstrom.com.forecast.models.Alert;
import android.zetterstrom.com.forecast.models.DataBlock;
import android.zetterstrom.com.forecast.models.DataPoint;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by abdulsamad on 16/03/2018.
 */

public class DataBlockConverter {
    private static Gson gson = new Gson();

    //    DataBlock
    @TypeConverter
    public static List<DataBlock> stringToDataBlockList(String data) {
        if (data == null) {
            return Collections.emptyList();
        }

        Type listType = new TypeToken<List<DataBlock>>() {
        }.getType();

        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public static String dataBlockListToString(List<DataBlock> dataBlocks) {
        return gson.toJson(dataBlocks);
    }
}
