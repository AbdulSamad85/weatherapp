package android.zetterstrom.com.forecast.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.zetterstrom.com.forecast.dao.ForecastDao;
import android.zetterstrom.com.forecast.models.Alert;
import android.zetterstrom.com.forecast.models.DataBlock;
import android.zetterstrom.com.forecast.models.DataPoint;
import android.zetterstrom.com.forecast.models.Flags;
import android.zetterstrom.com.forecast.models.Forecast;

/**
 * Created by abdulsamad on 16/03/2018.
 */

@Database(entities = { Forecast.class, Alert.class, DataBlock.class, DataPoint.class, Flags.class}, version = 1)
public abstract class ForecastDB extends RoomDatabase {
    public abstract ForecastDao ForecastDao();
}

