package android.zetterstrom.com.forecast.typeconverts;

import android.arch.persistence.room.TypeConverter;
import android.zetterstrom.com.forecast.models.Unit;

/**
 * Created by abdulsamad on 17/03/2018.
 */

public class UnitTypeConverter {
    @TypeConverter
    public static Unit toStatus(String unit) {
        if (unit.equalsIgnoreCase(Unit.US.getText())) {
            return Unit.US;
        } else if (unit.equalsIgnoreCase(Unit.SI.getText())) {
            return Unit.SI;
        } else if (unit.equalsIgnoreCase(Unit.CA.getText())) {
            return Unit.CA;
        } else if (unit.equalsIgnoreCase(Unit.UK.getText())) {
            return Unit.UK;
        } else if (unit.equalsIgnoreCase(Unit.UK2.getText())) {
            return Unit.UK2;
        } else if (unit.equalsIgnoreCase(Unit.AUTO.getText())) {
            return Unit.AUTO;
        } else {
            throw new IllegalArgumentException("Could not recognize unit");
        }
    }

    @TypeConverter
    public static String toString(Unit unit) {
        return unit.getText();
    }
}
