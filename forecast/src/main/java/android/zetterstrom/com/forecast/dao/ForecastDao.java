package android.zetterstrom.com.forecast.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import android.zetterstrom.com.forecast.models.Forecast;

import java.util.List;

/**
 * Created by abdulsamad on 16/03/2018.
 */

@Dao
public interface ForecastDao {
    @Query("SELECT * FROM Forecast")
    List<Forecast> getAll();

    @Query("SELECT * FROM Forecast WHERE FORECAST_ID = :id")
    Forecast findById(int id);

    @Query("SELECT * FROM Forecast WHERE FIELD_LOCATION = :locationName")
    Forecast findByLocation(String locationName);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Forecast> forecasts);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Forecast forecasts);

    @Delete
    void delete(Forecast forecast);
}
