package android.zetterstrom.com.forecast.typeconverts;

import android.arch.persistence.room.TypeConverter;

import java.util.Date;

/**
 * Created by abdulsamad on 17/03/2018.
 */

public class DateConverter {
    @TypeConverter
    public static Date toDate(Long timestamp) {
        return timestamp == null ? null : new Date(timestamp);
    }

    @TypeConverter
    public static Long toTimestamp(Date date) {
        return date == null ? null : date.getTime();
    }
}
