/*
 * Copyright 2016 Kevin Zetterstrom
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package android.zetterstrom.com.forecast.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.Nullable;
import android.zetterstrom.com.forecast.typeconverts.AlertConverter;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * In general, to determine the weather at a given point in time, one should examine the highest-precision
 * data block defined (minutely, hourly, and daily respectively), taking any data available from from it
 * and falling back to the next-highest precision data block for any properties that are missing for the
 * point in time desired.
 * <p/>
 * Not all fields may contain data. A null field is indicative that it was not present in the response.
 * <p/>
 * <p/>
 * Created by Kevin Zetterstrom on 2/10/16.
 */
@SuppressWarnings("unused")
@Entity(indices = {@Index(value = {ModelConstants.FIELD_LOCATION},
        unique = true)})
public class Forecast implements Serializable {

    private static final long serialVersionUID = -2568653365791715874L;

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = ModelConstants.FORECAST_ID)
    private int id;

    @SerializedName(ModelConstants.FIELD_LATITUDE)
    @ColumnInfo(name = ModelConstants.FIELD_LATITUDE)
    private double latitude;

    @SerializedName(ModelConstants.FIELD_LONGITUDE)
    @ColumnInfo(name = ModelConstants.FIELD_LONGITUDE)
    private double longitude;

    @SerializedName(ModelConstants.FIELD_TIMEZONE)
    @ColumnInfo(name = ModelConstants.FIELD_TIMEZONE)
    private String timezone;

    @ColumnInfo(name = ModelConstants.FIELD_LOCATION)
    private String location;

    @SerializedName(ModelConstants.FIELD_OFFSET)
    @ColumnInfo(name = ModelConstants.FIELD_OFFSET)
    private double offset;

    @Nullable
    @SerializedName(ModelConstants.FIELD_CURRENTLY)
    @Embedded(prefix = ModelConstants.FIELD_CURRENTLY)
    private DataPoint currently;

    @Nullable
    @SerializedName(ModelConstants.FIELD_MINUTELY)
    @Embedded(prefix = ModelConstants.FIELD_MINUTELY)
    private DataBlock minutely;

    @Nullable
    @SerializedName(ModelConstants.FIELD_HOURLY)
    @Embedded(prefix = ModelConstants.FIELD_HOURLY)
    private DataBlock hourly;

    @Nullable
    @SerializedName(ModelConstants.FIELD_DAILY)
    @Embedded(prefix = ModelConstants.FIELD_DAILY)
    private DataBlock daily;

    @Nullable
    @SerializedName(ModelConstants.FIELD_ALERTS)
    @ColumnInfo(name = ModelConstants.FIELD_ALERTS)
    @TypeConverters(AlertConverter.class)
    private List<Alert> alerts;

    @Nullable
    @SerializedName(ModelConstants.FIELD_FLAGS)
    @Embedded
    private Flags flags;

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getTimezone() {
        return timezone;
    }

    public double getOffset() {
        return offset;
    }

    @Nullable
    public DataPoint getCurrently() {
        return currently;
    }

    @Nullable
    public DataBlock getMinutely() {
        return minutely;
    }

    @Nullable
    public DataBlock getHourly() {
        return hourly;
    }

    @Nullable
    public DataBlock getDaily() {
        return daily;
    }

    @Nullable
    public List<Alert> getAlerts() {
        return alerts;
    }

    @Nullable
    public Flags getFlags() {
        return flags;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public void setOffset(double offset) {
        this.offset = offset;
    }

    public void setCurrently(@Nullable DataPoint currently) {
        this.currently = currently;
    }

    public void setMinutely(@Nullable DataBlock minutely) {
        this.minutely = minutely;
    }

    public void setHourly(@Nullable DataBlock hourly) {
        this.hourly = hourly;
    }

    public void setDaily(@Nullable DataBlock daily) {
        this.daily = daily;
    }

    public void setAlerts(@Nullable List<Alert> alerts) {
        this.alerts = alerts;
    }

    public void setFlags(@Nullable Flags flags) {
        this.flags = flags;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
