package android.zetterstrom.com.forecast.typeconverts;

import android.arch.persistence.room.TypeConverter;
import android.zetterstrom.com.forecast.models.DataBlock;
import android.zetterstrom.com.forecast.models.DataPoint;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

/**
 * Created by abdulsamad on 16/03/2018.
 */

public class DataPointConverter {
    private static Gson gson = new Gson();

    //    DataPoint
    @TypeConverter
    public static List<DataPoint> stringToDataPointList(String data) {
        if (data == null) {
            return Collections.emptyList();
        }

        Type listType = new TypeToken<List<DataPoint>>() {
        }.getType();

        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public static String dataPointListToString(List<DataPoint> dataPoints) {
        return gson.toJson(dataPoints);
    }
}
