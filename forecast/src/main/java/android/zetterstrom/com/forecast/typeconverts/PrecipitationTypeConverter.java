package android.zetterstrom.com.forecast.typeconverts;

import android.arch.persistence.room.TypeConverter;
import android.zetterstrom.com.forecast.models.Icon;
import android.zetterstrom.com.forecast.models.PrecipitationType;

/**
 * Created by abdulsamad on 17/03/2018.
 */

public class PrecipitationTypeConverter {

    @TypeConverter
    public static PrecipitationType toStatus(String precipitationType) {
        if (precipitationType.equalsIgnoreCase(PrecipitationType.RAIN.getText())) {
            return PrecipitationType.RAIN;
        } else  if (precipitationType.equalsIgnoreCase(PrecipitationType.SLEET.getText())) {
            return PrecipitationType.SLEET;
        } else  if (precipitationType.equalsIgnoreCase(PrecipitationType.HAIL.getText())) {
            return PrecipitationType.HAIL;
        } else {
            throw new IllegalArgumentException("Could not recognize PrecipitationType");
        }
    }

    @TypeConverter
    public static String toString(PrecipitationType type) {
        return type.getText();
    }
}
