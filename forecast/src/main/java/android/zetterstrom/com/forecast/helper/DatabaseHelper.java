package android.zetterstrom.com.forecast.helper;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.zetterstrom.com.forecast.Constants;
import android.zetterstrom.com.forecast.database.ForecastDB;

/**
 * Created by abdulsamad on 17/03/2018.
 */

public class DatabaseHelper {
    private static ForecastDB instance;

    public static ForecastDB getInstance(Context context) {
        if(instance == null) {
            instance = Room.databaseBuilder(context, ForecastDB.class, Constants.FORECAST_DATABASE).allowMainThreadQueries().build();
        }
        return instance;
    }
}
