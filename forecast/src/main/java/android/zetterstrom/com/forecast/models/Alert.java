/*
 * Copyright 2016 Kevin Zetterstrom
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package android.zetterstrom.com.forecast.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.Nullable;
import android.zetterstrom.com.forecast.typeconverts.DateConverter;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

/**
 * An alert object represents a severe weather warning issued for the requested location by a
 * governmental authority
 * <p/>
 * Not all fields may contain data. A null field is indicative that it was not present in the response.
 * <p/>
 * <p/>
 * Created by Kevin Zetterstrom  on 2/10/16.
 */
@SuppressWarnings("unused")
@Entity
public class Alert implements Serializable {

    private static final long serialVersionUID = -4721384892605656941L;
    @PrimaryKey(autoGenerate = true)
    private int id;
    @Nullable
    @SerializedName(ModelConstants.FIELD_TITLE)
    @ColumnInfo(name = ModelConstants.FIELD_TITLE)
    private String title;

    @Nullable
    @SerializedName(ModelConstants.FIELD_DESCRIPTION)
    @ColumnInfo(name = ModelConstants.FIELD_DESCRIPTION)
    private String description;

    @Nullable
    @SerializedName(ModelConstants.FIELD_EXPIRES)
    @ColumnInfo(name = ModelConstants.FIELD_EXPIRES)
    @TypeConverters({DateConverter.class})
    private Date expires;

    @Nullable
    @SerializedName(ModelConstants.FIELD_URI)
    @ColumnInfo(name = ModelConstants.FIELD_URI)
    private String uri;

    @Nullable
    public String getTitle() {
        return title;
    }

    @Nullable
    public String getDescription() {
        return description;
    }

    @Nullable
    public Date getExpires() {
        return expires;
    }

    @Nullable
    public String getUri() {
        return uri;
    }

    public Alert() {
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public void setTitle(@Nullable String title) {
        this.title = title;
    }

    public void setDescription(@Nullable String description) {
        this.description = description;
    }

    public void setExpires(@Nullable Date expires) {
        this.expires = expires;
    }

    public void setUri(@Nullable String uri) {
        this.uri = uri;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
