package android.zetterstrom.com.forecast.typeconverts;

import android.arch.persistence.room.TypeConverter;
import android.zetterstrom.com.forecast.models.Icon;

/**
 * Created by abdulsamad on 17/03/2018.
 */

public class IconConverter {

    @TypeConverter
    public static Icon toStatus(String icon) {
        if (icon.equalsIgnoreCase(Icon.CLEAR_DAY.getText())) {
            return Icon.CLEAR_DAY;
        } else if (icon.equalsIgnoreCase(Icon.CLEAR_NIGHT.getText())) {
            return Icon.CLEAR_NIGHT;
        } else if (icon.equalsIgnoreCase(Icon.RAIN.getText())) {
            return Icon.RAIN;
        } else if (icon.equalsIgnoreCase(Icon.SNOW.getText())) {
            return Icon.SNOW;
        } else if (icon.equalsIgnoreCase(Icon.SLEET.getText())) {
            return Icon.SLEET;
        } else if (icon.equalsIgnoreCase(Icon.WIND.getText())) {
            return Icon.WIND;
        } else if (icon.equalsIgnoreCase(Icon.FOG.getText())) {
            return Icon.FOG;
        } else if (icon.equalsIgnoreCase(Icon.CLOUDY.getText())) {
            return Icon.CLOUDY;
        } else if (icon.equalsIgnoreCase(Icon.PARTLY_CLOUDY_DAY.getText())) {
            return Icon.PARTLY_CLOUDY_DAY;
        } else if (icon.equalsIgnoreCase(Icon.PARTLY_CLOUDY_NIGHT.getText())) {
            return Icon.PARTLY_CLOUDY_NIGHT;
        } else if (icon.equalsIgnoreCase(Icon.HAIL.getText())) {
            return Icon.HAIL;
        } else if (icon.equalsIgnoreCase(Icon.THUNDERSTORM.getText())) {
            return Icon.THUNDERSTORM;
        } else if (icon.equalsIgnoreCase(Icon.TORNADO.getText())) {
            return Icon.TORNADO;
        } else {
            throw new IllegalArgumentException("Could not recognize icon");
        }
    }

    @TypeConverter
    public static String toString(Icon icon) {
        return icon.getText();
    }
}
