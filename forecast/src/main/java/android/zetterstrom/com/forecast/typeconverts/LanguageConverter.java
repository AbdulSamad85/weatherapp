package android.zetterstrom.com.forecast.typeconverts;

import android.arch.persistence.room.TypeConverter;
import android.zetterstrom.com.forecast.models.Language;

/**
 * Created by abdulsamad on 17/03/2018.
 */

public class LanguageConverter {

    public static Language toStatus(String language) {
        if (language.equalsIgnoreCase(Language.TRADITIONAL_CHINESE.getText())) {
            return Language.TRADITIONAL_CHINESE;
        } else if (language.equalsIgnoreCase(Language.SIMPLIFIED_CHINESE.getText())) {
            return Language.SIMPLIFIED_CHINESE;
        } else if (language.equalsIgnoreCase(Language.PIG_LATIN.getText())) {
            return Language.PIG_LATIN;
        }else if (language.equalsIgnoreCase(Language.UKRAINIAN.getText())) {
            return Language.UKRAINIAN;
        }else if (language.equalsIgnoreCase(Language.TURKISH.getText())) {
            return Language.TURKISH;
        }else if (language.equalsIgnoreCase(Language.TETUM.getText())) {
            return Language.TETUM;
        }else if (language.equalsIgnoreCase(Language.SWEDISH.getText())) {
            return Language.SWEDISH;
        }else if (language.equalsIgnoreCase(Language.SERBIAN.getText())) {
            return Language.SERBIAN;
        }else if (language.equalsIgnoreCase(Language.SLOVAK.getText())) {
            return Language.SLOVAK;
        }else if (language.equalsIgnoreCase(Language.RUSSIAN.getText())) {
            return Language.RUSSIAN;
        }else if (language.equalsIgnoreCase(Language.PORTUGUESE.getText())) {
            return Language.PORTUGUESE;
        }else if (language.equalsIgnoreCase(Language.POLISH.getText())) {
            return Language.POLISH;
        }else if (language.equalsIgnoreCase(Language.DUTCH.getText())) {
            return Language.DUTCH;
        }else if (language.equalsIgnoreCase(Language.CORNISH.getText())) {
            return Language.CORNISH;
        }else if (language.equalsIgnoreCase(Language.ITALIAN.getText())) {
            return Language.ITALIAN;
        }else if (language.equalsIgnoreCase(Language.INDONESIAN.getText())) {
            return Language.INDONESIAN;
        }else if (language.equalsIgnoreCase(Language.HUNGARIAN.getText())) {
            return Language.HUNGARIAN;
        }else if (language.equalsIgnoreCase(Language.CROATIAN.getText())) {
            return Language.CROATIAN;
        }else if (language.equalsIgnoreCase(Language.FRENCH.getText())) {
            return Language.FRENCH;
        }else if (language.equalsIgnoreCase(Language.SPANISH.getText())) {
            return Language.SPANISH;
        }else if (language.equalsIgnoreCase(Language.ENGLISH.getText())) {
            return Language.ENGLISH;
        }else if (language.equalsIgnoreCase(Language.GREEK.getText())) {
            return Language.GREEK;
        }else if (language.equalsIgnoreCase(Language.GERMAN.getText())) {
            return Language.GERMAN;
        }else if (language.equalsIgnoreCase(Language.CZECH.getText())) {
            return Language.CZECH;
        }else if (language.equalsIgnoreCase(Language.BOSNIAN.getText())) {
            return Language.BOSNIAN;
        }else if (language.equalsIgnoreCase(Language.BELARUSIAN.getText())) {
            return Language.BELARUSIAN;
        }else if (language.equalsIgnoreCase(Language.AZERBAIJANI.getText())) {
            return Language.AZERBAIJANI;
        }else if (language.equalsIgnoreCase(Language.ARABIC.getText())) {
            return Language.ARABIC;
        } else {
            throw new IllegalArgumentException("Could not recognize language");
        }
    }

    @TypeConverter
    public static String toString(Language language) {
        return language.getText();
    }
}
