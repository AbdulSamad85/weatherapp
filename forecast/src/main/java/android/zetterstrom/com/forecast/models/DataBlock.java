/*
 * Copyright 2016 Kevin Zetterstrom
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package android.zetterstrom.com.forecast.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.Nullable;
import android.zetterstrom.com.forecast.typeconverts.DataPointConverter;
import android.zetterstrom.com.forecast.typeconverts.DateConverter;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * A data block object represents the various weather phenomena occurring over a period of time.
 * <p/>
 * Not all fields may contain data. A null field is indicative that it was not present in the response.
 * <p/>
 * <p/>
 * Created by Kevin Zetterstrom on 2/10/16.
 */
@SuppressWarnings("unused")
@Entity
public class DataBlock implements Serializable {

    private static final long serialVersionUID = 2215428778754516836L;

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = ModelConstants.DATABLOCK_ID)
    private int id;

    @Nullable
    @SerializedName(ModelConstants.FIELD_SUMMARY)
    @ColumnInfo(name = ModelConstants.DATABLOCK_FIELD_SUMMARY)
    private String summary;

    @Nullable
    @SerializedName(ModelConstants.FIELD_ICON)
    @ColumnInfo(name = ModelConstants.DATABLOCK_FIELD_ICON)
    private String icon;

    @Nullable
    @SerializedName(ModelConstants.FIELD_DATA)
    @ColumnInfo(name = ModelConstants.DATABLOCK_FIELD_DATA)
    @TypeConverters(DataPointConverter.class)
    private List<DataPoint> dataPoints;

    @Nullable
    public String getSummary() {
        return summary;
    }

    @Nullable
    public String getIcon() {
        return icon;
    }

    @Nullable
    public List<DataPoint> getDataPoints() {
        return dataPoints;
    }

    public DataBlock() {
    }

    public void setSummary(@Nullable String summary) {
        this.summary = summary;
    }

    public void setIcon(@Nullable String icon) {
        this.icon = icon;
    }

    public void setDataPoints(@Nullable List<DataPoint> dataPoints) {
        this.dataPoints = dataPoints;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
