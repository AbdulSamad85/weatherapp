/*
 * Copyright 2016 Kevin Zetterstrom
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package android.zetterstrom.com.forecast.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.Nullable;
import android.zetterstrom.com.forecast.typeconverts.DateConverter;
import android.zetterstrom.com.forecast.typeconverts.IconConverter;
import android.zetterstrom.com.forecast.typeconverts.PrecipitationTypeConverter;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

/**
 * A data point object represents the various weather phenomena occurring at a specific instant of time, and
 * has many varied properties. All of these properties (except time) are optional, and will only be set if
 * we have that type of information for that location and time. Please note that minutely data points are
 * always aligned to the nearest minute boundary, hourly points to the top of the hour, and daily points to
 * midnight of that day.
 * <p/>
 * Data points in the daily data block {@link DataBlock} are special: instead of representing the weather
 * phenomena at a given instant of time, they are an aggregate point representing the weather phenomena that
 * will occur over the entire day. For precipitation fields, this aggregate is a maximum; for other fields,
 * it is an average.
 * <p/>
 * Not all fields may contain data. A null field is indicative that it was not present in the response. This
 * is the reason {@link Double} is used as opposed to the primitive double.
 * <p/>
 * <p/>
 * Created by Kevin Zetterstrom  on 2/10/16.
 */
@SuppressWarnings("unused")
@Entity
public class DataPoint implements Serializable {

    private static final long serialVersionUID = 1089660797202113138L;

    @PrimaryKey(autoGenerate = true)
    private int id;

    @SerializedName(ModelConstants.FIELD_TIME)
    @ColumnInfo(name = ModelConstants.FIELD_TIME)
    @TypeConverters(DateConverter.class)
    private Date time;

    @Nullable
    @SerializedName(ModelConstants.FIELD_SUMMARY)
    @ColumnInfo(name = ModelConstants.DATAPOINT_FIELD_SUMMARY)
    private String summary;

    @Nullable
    @SerializedName(ModelConstants.FIELD_ICON)
    @ColumnInfo(name = ModelConstants.FIELD_ICON)
    @TypeConverters(IconConverter.class)
    private Icon icon;

    @Nullable
    @SerializedName(ModelConstants.FIELD_SUNSET_TIME)
    @ColumnInfo(name = ModelConstants.FIELD_SUNSET_TIME)
    @TypeConverters({DateConverter.class})
    private Date sunsetTime;

    @Nullable
    @SerializedName(ModelConstants.FIELD_SUNRISE_TIME)
    @ColumnInfo(name = ModelConstants.FIELD_SUNRISE_TIME)
    @TypeConverters({DateConverter.class})
    private Date sunriseTime;

    @Nullable
    @SerializedName(ModelConstants.FIELD_MOON_PHASE)
    @ColumnInfo(name = ModelConstants.FIELD_MOON_PHASE)
    private Double moonPhase;

    @Nullable
    @SerializedName(ModelConstants.FIELD_NEAREST_STORM_DISTANCE)
    @ColumnInfo(name = ModelConstants.FIELD_NEAREST_STORM_DISTANCE)
    private Double nearestStormDistance;

    @Nullable
    @SerializedName(ModelConstants.FIELD_NEAREST_STORM_BEARING)
    @ColumnInfo(name = ModelConstants.FIELD_NEAREST_STORM_BEARING)
    private Double nearestStormBearing;

    @Nullable
    @SerializedName(ModelConstants.FIELD_PRECIP_INTENSITY)
    @ColumnInfo(name = ModelConstants.FIELD_PRECIP_INTENSITY)
    private Double precipIntensity;

    @Nullable
    @SerializedName(ModelConstants.FIELD_PRECIP_INTENSITY_MAX)
    @ColumnInfo(name = ModelConstants.FIELD_PRECIP_INTENSITY_MAX)
    private Double precipIntensityMax;

    @Nullable
    @SerializedName(ModelConstants.FIELD_PRECIP_INTENSITY_MAX_TIME)
    @TypeConverters({DateConverter.class})
    @ColumnInfo(name = ModelConstants.FIELD_PRECIP_INTENSITY_MAX_TIME)
    private Date precipIntensityMaxTime;

    @Nullable
    @SerializedName(ModelConstants.FIELD_PRECIP_PROBABILITY)
    @ColumnInfo(name = ModelConstants.FIELD_PRECIP_PROBABILITY)
    private Double precipProbability;

    @Nullable
    @SerializedName(ModelConstants.FIELD_PRECIP_TYPE)
    @ColumnInfo(name = ModelConstants.FIELD_PRECIP_TYPE)
    @TypeConverters(PrecipitationTypeConverter.class)
    private PrecipitationType precipitationType;

    @Nullable
    @SerializedName(ModelConstants.FIELD_PRECIP_ACCUMULATION)
    @ColumnInfo(name = ModelConstants.FIELD_PRECIP_ACCUMULATION)
    private Double precipAccumulation;

    @Nullable
    @SerializedName(ModelConstants.FIELD_TEMPERATURE)
    @ColumnInfo(name = ModelConstants.FIELD_TEMPERATURE)
    private Double temperature;

    @Nullable
    @SerializedName(ModelConstants.FIELD_TEMPERATURE_MIN)
    @ColumnInfo(name = ModelConstants.FIELD_TEMPERATURE_MIN)
    private Double temperatureMin;

    @Nullable
    @SerializedName(ModelConstants.FIELD_TEMPERATURE_MIN_TIME)
    @ColumnInfo(name = ModelConstants.FIELD_TEMPERATURE_MIN_TIME)
    @TypeConverters({DateConverter.class})
    private Date temperatureMinTime;

    @Nullable
    @SerializedName(ModelConstants.FIELD_TEMPERATURE_MAX)
    @ColumnInfo(name = ModelConstants.FIELD_TEMPERATURE_MAX)
    private Double temperatureMax;

    @Nullable
    @SerializedName(ModelConstants.FIELD_TEMPERATURE_MAX_TIME)
    @ColumnInfo(name = ModelConstants.FIELD_TEMPERATURE_MAX_TIME)
    @TypeConverters({DateConverter.class})
    private Date temperatureMaxTime;

    @Nullable
    @SerializedName(ModelConstants.FIELD_APPARENT_TEMPERATURE)
    @ColumnInfo(name = ModelConstants.FIELD_APPARENT_TEMPERATURE)
    private Double apparentTemperature;

    @Nullable
    @SerializedName(ModelConstants.FIELD_APPARENT_TEMPERATURE_MIN)
    @ColumnInfo(name = ModelConstants.FIELD_APPARENT_TEMPERATURE_MIN)
    private Double apparentTemperatureMin;

    @Nullable
    @SerializedName(ModelConstants.FIELD_APPARENT_TEMPERATURE_MIN_TIME)
    @ColumnInfo(name = ModelConstants.FIELD_APPARENT_TEMPERATURE_MIN_TIME)
    @TypeConverters({DateConverter.class})
    private Date apparentTemperatureMinTime;

    @Nullable
    @SerializedName(ModelConstants.FIELD_APPARENT_TEMPERATURE_MAX)
    @ColumnInfo(name = ModelConstants.FIELD_APPARENT_TEMPERATURE_MAX)
    private Double apparentTemperatureMax;

    @Nullable
    @SerializedName(ModelConstants.FIELD_APPARENT_TEMPERATURE_MAX_TIME)
    @ColumnInfo(name = ModelConstants.FIELD_APPARENT_TEMPERATURE_MAX_TIME)
    @TypeConverters({DateConverter.class})
    private Date apparentTemperatureMaxTime;

    @Nullable
    @SerializedName(ModelConstants.FIELD_DEW_POINT)
    @ColumnInfo(name = ModelConstants.FIELD_DEW_POINT)
    private Double dewPoint;

    @Nullable
    @SerializedName(ModelConstants.FIELD_WIND_SPEED)
    @ColumnInfo(name = ModelConstants.FIELD_WIND_SPEED)
    private Double windSpeed;

    @Nullable
    @SerializedName(ModelConstants.FIELD_WIND_BEARING)
    @ColumnInfo(name = ModelConstants.FIELD_WIND_BEARING)
    private Double windBearing;

    @Nullable
    @SerializedName(ModelConstants.FIELD_CLOUD_COVER)
    @ColumnInfo(name = ModelConstants.FIELD_CLOUD_COVER)
    private Double cloudCover;

    @Nullable
    @SerializedName(ModelConstants.FIELD_HUMIDITY)
    @ColumnInfo(name = ModelConstants.FIELD_HUMIDITY)
    private Double humidity;

    @Nullable
    @SerializedName(ModelConstants.FIELD_PRESSURE)
    @ColumnInfo(name = ModelConstants.FIELD_PRESSURE)
    private Double pressure;

    @Nullable
    @SerializedName(ModelConstants.FIELD_VISIBILITY)
    @ColumnInfo(name = ModelConstants.FIELD_VISIBILITY)
    private Double visibility;

    @Nullable
    @SerializedName(ModelConstants.FIELD_OZONE)
    @ColumnInfo(name = ModelConstants.FIELD_OZONE)
    private Double ozone;

    public DataPoint() {
    }

    public Date getTime() {
        return time;
    }

    @Nullable
    public String getSummary() {
        return summary;
    }

    @Nullable
    public Icon getIcon() {
        return icon;
    }

    @Nullable
    public Date getSunsetTime() {
        return sunsetTime;
    }

    @Nullable
    public Date getSunriseTime() {
        return sunriseTime;
    }

    @Nullable
    public Double getMoonPhase() {
        return moonPhase;
    }


    public void setTime(Date time) {
        this.time = time;
    }

    public void setSummary(@Nullable String summary) {
        this.summary = summary;
    }

    public void setIcon(@Nullable Icon icon) {
        this.icon = icon;
    }

    public void setSunsetTime(@Nullable Date sunsetTime) {
        this.sunsetTime = sunsetTime;
    }

    public void setSunriseTime(@Nullable Date sunriseTime) {
        this.sunriseTime = sunriseTime;
    }

    public void setMoonPhase(@Nullable Double moonPhase) {
        this.moonPhase = moonPhase;
    }

    @Nullable
    public Double getNearestStormDistance() {
        return nearestStormDistance;
    }

    public void setNearestStormDistance(@Nullable Double nearestStormDistance) {
        this.nearestStormDistance = nearestStormDistance;
    }

    @Nullable
    public Double getNearestStormBearing() {
        return nearestStormBearing;
    }

    public void setNearestStormBearing(@Nullable Double nearestStormBearing) {
        this.nearestStormBearing = nearestStormBearing;
    }

    @Nullable
    public Double getPrecipIntensity() {
        return precipIntensity;
    }

    public void setPrecipIntensity(@Nullable Double precipIntensity) {
        this.precipIntensity = precipIntensity;
    }

    @Nullable
    public Double getPrecipIntensityMax() {
        return precipIntensityMax;
    }

    public void setPrecipIntensityMax(@Nullable Double precipIntensityMax) {
        this.precipIntensityMax = precipIntensityMax;
    }

    @Nullable
    public Date getPrecipIntensityMaxTime() {
        return precipIntensityMaxTime;
    }

    public void setPrecipIntensityMaxTime(@Nullable Date precipIntensityMaxTime) {
        this.precipIntensityMaxTime = precipIntensityMaxTime;
    }

    @Nullable
    public Double getPrecipProbability() {
        return precipProbability;
    }

    public void setPrecipProbability(@Nullable Double precipProbability) {
        this.precipProbability = precipProbability;
    }

    @Nullable
    public PrecipitationType getPrecipitationType() {
        return precipitationType;
    }

    public void setPrecipitationType(@Nullable PrecipitationType precipitationType) {
        this.precipitationType = precipitationType;
    }

    @Nullable
    public Double getPrecipAccumulation() {
        return precipAccumulation;
    }

    public void setPrecipAccumulation(@Nullable Double precipAccumulation) {
        this.precipAccumulation = precipAccumulation;
    }

    @Nullable
    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(@Nullable Double temperature) {
        this.temperature = temperature;
    }

    @Nullable
    public Double getTemperatureMin() {
        return temperatureMin;
    }

    public void setTemperatureMin(@Nullable Double temperatureMin) {
        this.temperatureMin = temperatureMin;
    }

    @Nullable
    public Date getTemperatureMinTime() {
        return temperatureMinTime;
    }

    public void setTemperatureMinTime(@Nullable Date temperatureMinTime) {
        this.temperatureMinTime = temperatureMinTime;
    }

    @Nullable
    public Double getTemperatureMax() {
        return temperatureMax;
    }

    public void setTemperatureMax(@Nullable Double temperatureMax) {
        this.temperatureMax = temperatureMax;
    }

    @Nullable
    public Date getTemperatureMaxTime() {
        return temperatureMaxTime;
    }

    public void setTemperatureMaxTime(@Nullable Date temperatureMaxTime) {
        this.temperatureMaxTime = temperatureMaxTime;
    }

    @Nullable
    public Double getApparentTemperature() {
        return apparentTemperature;
    }

    public void setApparentTemperature(@Nullable Double apparentTemperature) {
        this.apparentTemperature = apparentTemperature;
    }

    @Nullable
    public Double getApparentTemperatureMin() {
        return apparentTemperatureMin;
    }

    public void setApparentTemperatureMin(@Nullable Double apparentTemperatureMin) {
        this.apparentTemperatureMin = apparentTemperatureMin;
    }

    @Nullable
    public Date getApparentTemperatureMinTime() {
        return apparentTemperatureMinTime;
    }

    public void setApparentTemperatureMinTime(@Nullable Date apparentTemperatureMinTime) {
        this.apparentTemperatureMinTime = apparentTemperatureMinTime;
    }

    @Nullable
    public Double getApparentTemperatureMax() {
        return apparentTemperatureMax;
    }

    public void setApparentTemperatureMax(@Nullable Double apparentTemperatureMax) {
        this.apparentTemperatureMax = apparentTemperatureMax;
    }

    @Nullable
    public Date getApparentTemperatureMaxTime() {
        return apparentTemperatureMaxTime;
    }

    public void setApparentTemperatureMaxTime(@Nullable Date apparentTemperatureMaxTime) {
        this.apparentTemperatureMaxTime = apparentTemperatureMaxTime;
    }

    @Nullable
    public Double getDewPoint() {
        return dewPoint;
    }

    public void setDewPoint(@Nullable Double dewPoint) {
        this.dewPoint = dewPoint;
    }

    @Nullable
    public Double getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(@Nullable Double windSpeed) {
        this.windSpeed = windSpeed;
    }

    @Nullable
    public Double getWindBearing() {
        return windBearing;
    }

    public void setWindBearing(@Nullable Double windBearing) {
        this.windBearing = windBearing;
    }

    @Nullable
    public Double getCloudCover() {
        return cloudCover;
    }

    public void setCloudCover(@Nullable Double cloudCover) {
        this.cloudCover = cloudCover;
    }

    @Nullable
    public Double getHumidity() {
        return humidity;
    }

    public void setHumidity(@Nullable Double humidity) {
        this.humidity = humidity;
    }

    @Nullable
    public Double getPressure() {
        return pressure;
    }

    public void setPressure(@Nullable Double pressure) {
        this.pressure = pressure;
    }

    @Nullable
    public Double getVisibility() {
        return visibility;
    }

    public void setVisibility(@Nullable Double visibility) {
        this.visibility = visibility;
    }

    @Nullable
    public Double getOzone() {
        return ozone;
    }

    public void setOzone(@Nullable Double ozone) {
        this.ozone = ozone;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
