package com.echo.weatherapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.zetterstrom.com.forecast.models.Forecast;

import com.echo.weatherapp.adapter.DailyForecastListAdapter;
import com.echo.weatherapp.adapter.HourlyForecastListAdapter;
import com.echo.weatherapp.base.BaseFragment;
import com.echo.weatherapp.base.BaseFragmentPresenter;
import com.echo.weatherapp.databinding.LayoutPageWeatherForcastBinding;


/**
 * Created by abdulsamad on 28/08/2016.
 */

public class FragmentWeatherPage extends BaseFragment {

    private HourlyForecastListAdapter hourlyForecastListAdapter;
    private DailyForecastListAdapter dailyForecastListAdapter;
    protected static final String KEY_WEATHER_FORECAST = "KEY_WEATHER_FORECAST";
    protected static final String KEY_TITLE = "KEY_TITLE";
    protected static String title = "";
    private LayoutPageWeatherForcastBinding binding;
    private FragmentWeatherPagerPresenter presenter;

    // newInstance constructor for creating fragment with arguments
    public static FragmentWeatherPage newInstance(Forecast weatherForecast, String title) {
        FragmentWeatherPage fragmentWeatherPage = new FragmentWeatherPage();
        Bundle args = new Bundle();
        args.putSerializable(KEY_WEATHER_FORECAST, weatherForecast);
        args.putString(KEY_TITLE, title);
        fragmentWeatherPage.setArguments(args);
        return fragmentWeatherPage;
    }

    public FragmentWeatherPage() {
//        Empty constructor
    }

    @Override
    protected BaseFragmentPresenter getPresenter() {
        return presenter;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = LayoutPageWeatherForcastBinding.inflate(inflater, container, false);
        presenter = new FragmentWeatherPagerPresenter(this);
        initializeResources();
        return binding.getRoot();
    }

    private void initializeResources() {
        hourlyForecastListAdapter =
                new HourlyForecastListAdapter(getActivity(), presenter.weatherForecast.getHourly().getDataPoints());
        LinearLayoutManager horizontalManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, true);
        binding.hourlyRecyclerView.setLayoutManager(horizontalManager);
        binding.hourlyRecyclerView.setAdapter(hourlyForecastListAdapter);

        dailyForecastListAdapter =
                new DailyForecastListAdapter(getActivity(), presenter.weatherForecast.getDaily().getDataPoints());
        LinearLayoutManager manager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        binding.dailyRecyclerView.setLayoutManager(manager);
        binding.dailyRecyclerView.setAdapter(dailyForecastListAdapter);
    }

    protected void updateHourlyTemperatureView() {
        hourlyForecastListAdapter.notifyDataSetChanged();
    }

    protected void updateDailyTemperatureView() {
        dailyForecastListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.onViewCreated();
    }

    public static String getTitle() {
        return title;
    }
}
