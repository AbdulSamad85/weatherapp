package com.echo.weatherapp.fragment;

import android.zetterstrom.com.forecast.models.Forecast;

import com.echo.weatherapp.base.BaseFragmentPresenter;

/**
 * Created by abdulsamad on 19/03/2018.
 */

public class FragmentWeatherPagerPresenter extends BaseFragmentPresenter {
    private FragmentWeatherPage view;
    protected Forecast weatherForecast;
    public FragmentWeatherPagerPresenter(FragmentWeatherPage view) {
        this.view = view;
        handleBundle();
    }

    @Override
    protected void onDestroyView() {
        view = null;
    }

    private void handleBundle() {
        if (view.getArguments() != null) {
            weatherForecast = (Forecast) view.getArguments().getSerializable(FragmentWeatherPage.KEY_WEATHER_FORECAST);
            FragmentWeatherPage.title = view.getArguments().getString(FragmentWeatherPage.KEY_TITLE);
        }
    }

    protected void onViewCreated() {
        if (weatherForecast.getHourly() != null) {
            view.updateHourlyTemperatureView();
            view.updateDailyTemperatureView();
        }
    }
}
