package com.echo.weatherapp.model;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by abdulsamad on 19/03/2018.
 */

public class SearchedLocation {
    private String name;
    private LatLng coordinates;

    public SearchedLocation(String name, LatLng coordinates) {
        this.name = name;
        this.coordinates = coordinates;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LatLng getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(LatLng coordinates) {
        this.coordinates = coordinates;
    }
}
