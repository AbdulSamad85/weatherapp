package com.echo.weatherapp.application;

import android.app.Application;
import android.zetterstrom.com.forecast.Constants;
import android.zetterstrom.com.forecast.ForecastClient;
import android.zetterstrom.com.forecast.ForecastConfiguration;
import android.zetterstrom.com.forecast.helper.DatabaseHelper;
import com.echo.weatherapp.R;
import java.util.ArrayList;
import java.util.List;


public class WeatherApplication extends Application {


    @Override
    public void onCreate() {
        super.onCreate();
        initForecastClient();
        initRoom();
    }

    private void initRoom(){
        DatabaseHelper.getInstance(this);
    }

    private void initForecastClient() {
        List<String> excludeList = new ArrayList<>();
        excludeList.add(Constants.OPTIONS_EXCLUDE_CURRENLY);
        excludeList.add(Constants.OPTIONS_EXCLUDE_MINUTELY);
        excludeList.add(Constants.OPTIONS_EXCLUDE_ALERTS);
        excludeList.add(Constants.OPTIONS_EXCLUDE_FLAGS);

        ForecastConfiguration configuration =
                new ForecastConfiguration.Builder(getString(R.string.forecast_api_key)).setDefaultExcludeList(excludeList)
                        .setCacheDirectory(getCacheDir())
                        .build();
        ForecastClient.create(configuration);
    }

}