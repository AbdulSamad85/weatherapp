package com.echo.weatherapp.activity.addcity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by abdulsamad on 19/03/2018.
 */

public class ForceAddLocationActivity extends AddLocationActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding.addCurrentLocation.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        presenter.checkForLocationCount();
    }

    @Override
    protected void setResultToCaller(String name, LatLng latLng) {
        Bundle bundle = new Bundle();
        bundle.putString(AddLocationActivityPresenter.KEY_LOCATION_NAME, name);
        bundle.putParcelable(AddLocationActivityPresenter.KEY_LAT_LNG, latLng);
        navigateToSliderScreen(bundle);
    }
}
