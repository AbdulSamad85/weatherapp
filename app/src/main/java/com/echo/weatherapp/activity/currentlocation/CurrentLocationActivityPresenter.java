package com.echo.weatherapp.activity.currentlocation;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;

import com.echo.weatherapp.R;
import com.echo.weatherapp.activity.addcity.AddLocationActivityPresenter;
import com.echo.weatherapp.activity.slider.SliderActivity;
import com.echo.weatherapp.base.BaseActivityPresenter;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by abdulsamad on 19/03/2018.
 */

public class CurrentLocationActivityPresenter extends BaseActivityPresenter {

    private CurrentLocationActivity view;

    public CurrentLocationActivityPresenter(CurrentLocationActivity view) {
        this.view = view;
        if (!view.isLocationPermissionGranted()) {
            view.showLocationAccessNote();
        }
        handleBundle();
    }

    private void handleBundle() {
        Bundle bundle = view.getIntent().getExtras();
        if (bundle != null && bundle.containsKey(CurrentLocationActivity.KEY_AUTO_SHOW_LOCATION_PERMISSION)) {
            boolean askForPermission = bundle.getBoolean(CurrentLocationActivity.KEY_AUTO_SHOW_LOCATION_PERMISSION);
            if (askForPermission) {
                onAllowAccessClicked();
            }
        }
    }

    @Override
    protected void onDestroyView() {
        view = null;
    }

    public void onAllowAccessClicked() {
        view.requestForLocationPermission();
    }

    public void onRemoveCurrentLocationClicked() {
        view.navigateToAddLocationScreen();
    }

    public void sendToSliderActivity(Location location) {
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

        Intent intent = new Intent(view, SliderActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(AddLocationActivityPresenter.KEY_LOCATION_NAME, view.getString(R.string.current_location));
        bundle.putParcelable(AddLocationActivityPresenter.KEY_LAT_LNG, latLng);
        intent.putExtras(bundle);
        view.navigateToSliderScreen(intent);
    }
}
