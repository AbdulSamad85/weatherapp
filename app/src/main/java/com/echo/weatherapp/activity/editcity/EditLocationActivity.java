package com.echo.weatherapp.activity.editcity;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.zetterstrom.com.forecast.models.Forecast;

import com.echo.weatherapp.R;
import com.echo.weatherapp.activity.addcity.ForceAddLocationActivity;
import com.echo.weatherapp.adapter.SavedLocationListAdapter;
import com.echo.weatherapp.base.BaseActivity;
import com.echo.weatherapp.base.BaseActivityPresenter;
import com.echo.weatherapp.databinding.LayoutActivityEditLocationBinding;
import com.echo.weatherapp.interfaces.ILocationRemovalListener;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by abdulsamad on 03/09/2016.
 */

public class EditLocationActivity extends BaseActivity implements ILocationRemovalListener {

    public static final String KEY_SHOULD_RELOAD_DATA = "KEY_SHOULD_RELOAD_DATA";

    private SavedLocationListAdapter mAdapter;
    protected List<Forecast> forecasts = new ArrayList<>();
    private EditLocationActivityPresenter presenter;
    private LayoutActivityEditLocationBinding binding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.layout_activity_edit_location);
        presenter = new EditLocationActivityPresenter(this);
        initializeResource();
        initSavedLocationList();
    }

    private void initializeResource() {
        LinearLayoutManager manager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        binding.recyclerView.setLayoutManager(manager);
        mAdapter = new SavedLocationListAdapter(this, this, forecasts);
        binding.recyclerView.setAdapter(mAdapter);
    }

    @Override
    protected BaseActivityPresenter getPresenter() {
        return presenter;
    }

    private void initSavedLocationList() {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                forecasts.clear();
                forecasts.addAll(dataManager.getSavedForecasts());
                updateUi();
            }
        });
    }

    private void updateUi() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onLocationRemoved(final Forecast forecast) {
        dataManager.removeLocationForecastDownloadTime(forecast.getLocation());
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                dataManager.deleteForecast(forecast);
            }
        });
        Bundle bundle = new Bundle();
        bundle.putBoolean(KEY_SHOULD_RELOAD_DATA, true);
        Intent intent = new Intent();
        intent.putExtras(bundle);
        setResult(Activity.RESULT_OK, intent);
    }

    @Override
    public void onBackPressed() {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                List<Forecast> forecasts = dataManager.getSavedForecasts();
                if(forecasts == null || forecasts.size() == 0){
                    navigateToAddLocationScreen();
                }else {
                    finish();
                }
            }
        });
    }

    private void navigateToAddLocationScreen(){
        Intent intent = new Intent(this, ForceAddLocationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
