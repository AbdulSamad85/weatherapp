package com.echo.weatherapp.activity.addcity;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;
import android.zetterstrom.com.forecast.models.Forecast;

import com.echo.weatherapp.R;
import com.echo.weatherapp.base.BaseActivityPresenter;
import com.echo.weatherapp.interfaces.GetSavedLocationsCallback;
import com.echo.weatherapp.utility.DialogUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abdulsamad on 19/03/2018.
 */

public class AddLocationActivityPresenter extends BaseActivityPresenter implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    private AddLocationActivity view;
    public static final String KEY_LOCATION_NAME = "KEY_PLACE_NAME";
    public static final String KEY_LAT_LNG = "KEY_LAT_LNG";
    protected List<Forecast> savedForecasts = new ArrayList<>();

    public AddLocationActivityPresenter(AddLocationActivity view) {
        this.view = view;
    }

    protected void manageAddCurrentLocationVisibility(){
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                Forecast forecast = view.getDataManager().getSavedForecast(view.getString(R.string.current_location));
                if(forecast == null){
                    view.showAddCurrentLocationView();
                } else {
                    view.hideAddCurrentLocationView();
                }
            }
        });
    }

    @Override
    protected void onDestroyView() {
        view = null;
    }

    protected void initGoogleApiClient(){
        GoogleApiClient mGoogleApiClient = new GoogleApiClient
                .Builder(view)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(view, this)
                .build();
        mGoogleApiClient.registerConnectionCallbacks(this);
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        String message = "Failed to connect with server, please try again later";
        view.showToast(message);
        view.finishActivity();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                view.getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                if (!isLocationAlreadySaved(place.getName().toString())) {
                    view.setResultToCaller(place.getName().toString(), place.getLatLng());
                } else {
                    DialogUtils.showToast(view, view.getString(R.string.location_already_added));
                }
            }
            @Override
            public void onError(Status status) {
                String message = "Failed to find the place, please try again";
                view.showToast(message);
                Log.i("****", "An error occurred: " + status);
            }
        });

    }

    private boolean isLocationAlreadySaved(String location) {
        if (savedForecasts != null && savedForecasts.size() > 0) {
            for (Forecast forecast : savedForecasts) {
                if (forecast.getLocation().equalsIgnoreCase(location)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void onConnectionSuspended(int i) {
        String message = "Failed to connect with server, please try again later";
        view.showToast(message);
        view.finishActivity();
    }


    protected void populateSavedLocations(){
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                savedForecasts = view.getDataManager().getSavedForecasts();
            }
        });
    }

    protected void getAddedLocationCount(final GetSavedLocationsCallback callback){
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                List<Forecast> savedForecasts  = view.getDataManager().getSavedForecasts();
                callback.onLocationsRetrieved(savedForecasts);
            }
        });
    }

    protected void checkForLocationCount(){
        getAddedLocationCount(new GetSavedLocationsCallback() {
            @Override
            public void onLocationsRetrieved(List<Forecast> forecasts) {
                if(forecasts != null && forecasts.size()>0){
                    view.navigateToSliderScreen();
                } else {
                    String message = "Please add at least one location to proceed";
                    view.showMessage(message);
                }
            }
        });
    }

    public void onAddCurrentLocationClicked(){
        view.navigateToCurrentLocationScreen();
    }
}
