package com.echo.weatherapp.activity.slider;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Toast;
import android.zetterstrom.com.forecast.Constants;
import android.zetterstrom.com.forecast.ForecastClient;
import android.zetterstrom.com.forecast.models.Forecast;

import com.echo.weatherapp.R;
import com.echo.weatherapp.activity.addcity.AddLocationActivityPresenter;
import com.echo.weatherapp.adapter.WeatherPagerAdapter;
import com.echo.weatherapp.base.BaseActivityPresenter;
import com.echo.weatherapp.fragment.FragmentWeatherPage;
import com.echo.weatherapp.model.SearchedLocation;
import com.echo.weatherapp.utility.DialogUtils;
import com.echo.weatherapp.utility.NetworkUtils;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by abdulsamad on 19/03/2018.
 */

public class SliderActivityPresenter extends BaseActivityPresenter {
    private SliderActivity view;
    protected WeatherPagerAdapter adapter;
    protected ArrayList<Forecast> forecastList = new ArrayList<>();
    protected ArrayList<FragmentWeatherPage> weatherPageList = new ArrayList<>();
    protected ArrayList<String> titles = new ArrayList<>();
    protected LinkedHashMap<String, Forecast> forecastMap;
    protected ArrayList<Forecast> loadableForecastList;


    public SliderActivityPresenter(SliderActivity view) {
        this.view = view;
    }

    @Override
    protected void onDestroyView() {
        view = null;
    }

    protected void initializeResources() {
        loadableForecastList = new ArrayList<>();
        forecastMap = new LinkedHashMap<>();
        forecastList = new ArrayList<>();
        weatherPageList = new ArrayList<>();
        titles = new ArrayList<>();
        adapter = new WeatherPagerAdapter(view.getSupportFragmentManager(), titles, forecastList);
        view.initializeResources();
    }


    protected void loadForecasts() {
        forecastList.clear();
        titles.clear();
//        AsyncTask.execute(new Runnable() {
//            @Override
//            public void run() {
        List<Forecast> savedForecasts = view.getDataManager().getSavedForecasts();
        if (savedForecasts != null && savedForecasts.size() > 0) {
            for (Forecast item : savedForecasts) {
                if (view.getDataManager().shouldDownloadForecast(item.getLocation())) {
                    loadableForecastList.add(item);
                }else {
                    addForecastToUi(item);
                }
            }
        }
        if (loadableForecastList.size() > 0) {
            Forecast item = loadableForecastList.get(0);
            LatLng latLng = new LatLng(item.getLatitude(), item.getLongitude());
            view.showLoadingIndicator();
            getWeatherForecast(0, item.getLocation(), latLng);
        } else {
            SearchedLocation searchedLocation = getLocationFromBundle();
            if (searchedLocation != null) {
                view.showLoadingIndicator();
                getWeatherForecast(-1, searchedLocation.getName(), searchedLocation.getCoordinates());
            }
        }
//        });
    }


    private void addForecastToUi(Forecast forecast){
        forecastList.add(forecast);
        titles.add(forecast.getLocation());
        adapter.notifyDataSetChanged();
    }
    protected void getWeatherForecast(final int currentlyLoadingForecastIndex, final String location, final LatLng latLng) {

        List<String> exclusionList = new ArrayList<>();
        exclusionList.add(Constants.OPTIONS_EXCLUDE_ALERTS);
        exclusionList.add(Constants.OPTIONS_EXCLUDE_CURRENLY);
        exclusionList.add(Constants.OPTIONS_EXCLUDE_FLAGS);
        exclusionList.add(Constants.OPTIONS_EXCLUDE_MINUTELY);

        ForecastClient.getInstance()
                .getForecast(latLng.latitude, latLng.longitude, null, null, null, exclusionList, false, new Callback<Forecast>() {
                    @Override
                    public void onResponse(Call<Forecast> forecastCall, Response<Forecast> response) {
                        if (response.isSuccessful()) {
                            final Forecast forecast = response.body();
                            forecast.setLocation(location);
//                                TODO remove savedDownloadedTime when a forecast is removed form editLocation screen
                            view.getDataManager().saveLocationForecastDownloadTime(location, System.currentTimeMillis());

                            //updating viewPager
//                            forecastList.add(forecast);
//                            titles.add(location);
//                            adapter.notifyDataSetChanged();
                            addForecastToUi(forecast);
                            view.viewPager.setOffscreenPageLimit(forecastList.size());
                            // dumping to database
                            AsyncTask.execute(new Runnable() {
                                @Override
                                public void run() {
                                    view.getDataManager().saveForecast(forecast);
                                }
                            });

                            if (currentlyLoadingForecastIndex != -1) {
                                loadableForecastList.remove(currentlyLoadingForecastIndex);
                            }
                            //loading until all location forecasts are loaded
                            if (loadableForecastList.size() > 0) {
                                Forecast tobeLoadedForecast = loadableForecastList.get(0);
                                String location = tobeLoadedForecast.getLocation();
                                LatLng coordinates = new LatLng(tobeLoadedForecast.getLatitude(), tobeLoadedForecast.getLongitude());
                                getWeatherForecast(0, location, coordinates);
                            } else if (getLocationFromBundle() != null) {
                                SearchedLocation searchedLocation = getLocationFromBundle();
                                getWeatherForecast(-1, searchedLocation.getName(), searchedLocation.getCoordinates());

                            } else {
                                view.hideLoadingIndicator();
                                // updaing viewpager
                                view.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        view.viewPager.setCurrentItem(titles.indexOf(location));
                                        view.setTitle(location);
                                    }
                                });
                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<Forecast> forecastCall, Throwable t) {
                        if (loadableForecastList.size() == 0) {
                            view.hideLoadingIndicator();
                        } else {
                            DialogUtils.showToast(view, "Failed to load forecast for " + loadableForecastList.get(0).getLocation() + " please try again later");
                            if(NetworkUtils.isNetworkAvailable(view)) {
                                Forecast tobeLoadedForecast = loadableForecastList.get(0);
                                String location = tobeLoadedForecast.getLocation();
                                view.showLoadingIndicator();
                                LatLng coordinates = new LatLng(tobeLoadedForecast.getLatitude(), tobeLoadedForecast.getLongitude());
                                getWeatherForecast(0, location, coordinates);
                            } else {
                                view.hideLoadingIndicator();
                            }
                        }
                    }
                });
    }

    protected void parseActivityResultData(int requestCode, int resultCode, Intent data) {
        if (requestCode == SliderActivity.REQUEST_CODE_ADD_CITY && resultCode == Activity.RESULT_OK) {
            String city = data.getExtras().getString(AddLocationActivityPresenter.KEY_LOCATION_NAME);
            LatLng latLng = data.getExtras().getParcelable(AddLocationActivityPresenter.KEY_LAT_LNG);
            view.showLoadingIndicator();
            getWeatherForecast(-1, city, latLng);
        } else if (requestCode == SliderActivity.REQUEST_CODE_REMOVE_CITY && resultCode == Activity.RESULT_OK) {
            // reloading data
            loadForecasts();
        }
    }

    protected void refreshForecast() {
        if (adapter.getTitles().size() == 0) {
            view.hideLoadingIndicator();
            return;
        }
        final String locationName = adapter.getTitles().get(view.viewPager.getCurrentItem());

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                Forecast targetForecast = view.getDataManager().getSavedForecast(locationName);
                view.getDataManager().deleteForecast(targetForecast);
                view.getDataManager().removeLocationForecastDownloadTime(locationName);
                forecastList.remove(view.viewPager.getCurrentItem());
                titles.remove(view.viewPager.getCurrentItem());
                view.showLoadingIndicator();
                getWeatherForecast(-1, locationName, new LatLng(targetForecast.getLatitude(), targetForecast.getLongitude()));
            }
        });
    }

    private SearchedLocation getLocationFromBundle() {
        LatLng latLng = null;
        String name = null;
        Bundle bundle = view.getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(AddLocationActivityPresenter.KEY_LAT_LNG)) {
                latLng = bundle.getParcelable(AddLocationActivityPresenter.KEY_LAT_LNG);
            }

            if (bundle.containsKey(AddLocationActivityPresenter.KEY_LOCATION_NAME)) {
                name = bundle.getString(AddLocationActivityPresenter.KEY_LOCATION_NAME);
            }
        }
        if (name == null || latLng == null) {
            return null;
        } else {
            // clearing the bundle
            view.getIntent().removeExtra(AddLocationActivityPresenter.KEY_LAT_LNG);
            view.getIntent().removeExtra(AddLocationActivityPresenter.KEY_LOCATION_NAME);
        }
        return new SearchedLocation(name, latLng);
    }


}
