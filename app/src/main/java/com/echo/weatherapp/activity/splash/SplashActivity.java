package com.echo.weatherapp.activity.splash;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.zetterstrom.com.forecast.models.Forecast;

import com.echo.weatherapp.R;
import com.echo.weatherapp.activity.addcity.ForceAddLocationActivity;
import com.echo.weatherapp.activity.slider.SliderActivity;
import com.echo.weatherapp.base.BaseActivity;
import com.echo.weatherapp.base.BaseActivityPresenter;
import com.echo.weatherapp.databinding.SplashActivityLayoutBinding;

import java.util.List;

/**
 * Created by AbdulSamad on 1/15/2018.
 */

public class SplashActivity extends BaseActivity {

    private static final int ANIMATION_DURATION = 1500;
    private SplashActivityLayoutBinding binding;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.splash_activity_layout);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //to remove the action bar (title bar)
        if(getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        new Handler().postDelayed(new Runnable() {
            public void run() {
             navigateToSliderActivity();
            }
        }, ANIMATION_DURATION);
        animateLogo();
    }

    @Override
    protected BaseActivityPresenter getPresenter() {
        return null;
    }

    private void animateLogo(){
        binding.logo.startAnimation(AnimationUtils.loadAnimation(this, R.anim.logo_animation));
    }
    private void navigateToSliderActivity(){
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                List<Forecast> forecasts = dataManager.getSavedForecasts();
                Intent intent;
                if(forecasts != null && forecasts.size()>0){
                    intent = new Intent(SplashActivity.this, SliderActivity.class);
                } else {
                    intent = new Intent(SplashActivity.this, ForceAddLocationActivity.class);
                }
                startActivity(intent);
                overridePendingTransition(R.anim.splash_fade_in, R.anim.splash_fade_out);
                finish();
            }
        });

    }
}
