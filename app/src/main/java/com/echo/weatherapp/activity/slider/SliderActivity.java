package com.echo.weatherapp.activity.slider;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import com.echo.weatherapp.R;
import com.echo.weatherapp.activity.addcity.AddLocationActivity;
import com.echo.weatherapp.activity.editcity.EditLocationActivity;
import com.echo.weatherapp.base.BaseActivity;
import com.echo.weatherapp.base.BaseActivityPresenter;
import com.echo.weatherapp.databinding.LayoutSliderActivityBinding;
import com.echo.weatherapp.utility.DataManager;
import com.echo.weatherapp.utility.NetworkUtils;

public class SliderActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, SwipeRefreshLayout.OnRefreshListener {

    protected static final int REQUEST_CODE_ADD_CITY = 1001;
    protected static final int REQUEST_CODE_REMOVE_CITY = 1002;
    protected SliderActivityPresenter presenter;
    protected LayoutSliderActivityBinding binding;
    protected SwipeRefreshLayout mSwipeRefreshLayout;
    protected ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.layout_slider_activity);
        presenter = new SliderActivityPresenter(this);
        presenter.initializeResources();
        presenter.loadForecasts();
    }

    protected void initializeResources() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, binding.drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        binding.drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        binding.navView.setNavigationItemSelectedListener(this);

        viewPager = findViewById(R.id.vp_cityWeatherPager);
        mSwipeRefreshLayout = findViewById(R.id.refresh);
        mSwipeRefreshLayout.setOnRefreshListener(SliderActivity.this);


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                setTitle(presenter.titles.get(position));
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                enableDisableSwipeRefresh(state == ViewPager.SCROLL_STATE_IDLE);
            }
        });
        viewPager.setAdapter(presenter.adapter);
    }

    @Override
    protected BaseActivityPresenter getPresenter() {
        return presenter;
    }


    public DataManager getDataManager() {
        return dataManager;
    }

    protected void enableDisableSwipeRefresh(boolean enable) {
        mSwipeRefreshLayout.setEnabled(enable);
    }


    protected void hideLoadingIndicator() {
        runOnUiThread(new Runnable() {
            public void run() {
                if (mSwipeRefreshLayout.isRefreshing()) {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            }
        });

    }

    protected void showLoadingIndicator() {
        runOnUiThread(new Runnable() {
            public void run() {
                if (!mSwipeRefreshLayout.isRefreshing()) {
                    mSwipeRefreshLayout.setRefreshing(true);
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.slider, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add_location) {
            navigateToAddCityActivity();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void navigateToAddCityActivity() {
        Intent intent = new Intent(SliderActivity.this, AddLocationActivity.class);
        startActivityForResult(intent, REQUEST_CODE_ADD_CITY);
    }

    protected void navigateToEditCityActivity() {
        Intent intent = new Intent(SliderActivity.this, EditLocationActivity.class);
        startActivityForResult(intent, REQUEST_CODE_REMOVE_CITY);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_edit_location) {
            navigateToEditCityActivity();
        }
        closeDrawer();
        return true;
    }

    private void closeDrawer() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        presenter.parseActivityResultData(requestCode, resultCode, data);
    }


    @Override
    public void onRefresh() {
        if(NetworkUtils.isNetworkAvailable(this)) {
            presenter.refreshForecast();
        }else {
            hideLoadingIndicator();
        }
    }
}
