package com.echo.weatherapp.activity.currentlocation;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.echo.weatherapp.R;
import com.echo.weatherapp.activity.addcity.AddLocationActivity;
import com.echo.weatherapp.activity.addcity.ForceAddLocationActivity;
import com.echo.weatherapp.base.BaseActivityPresenter;
import com.echo.weatherapp.base.BaseLocationActivity;
import com.echo.weatherapp.databinding.CurrentLocationLayoutBinding;

/**
 * Created by abdulsamad on 19/03/2018.
 */

public class CurrentLocationActivity extends BaseLocationActivity {

    public static final String KEY_AUTO_SHOW_LOCATION_PERMISSION = "KEY_AUTO_SHOW_LOCATION_PERMISSION";
    protected CurrentLocationActivityPresenter presenter;
    protected CurrentLocationLayoutBinding binding;

    public static Intent create(Context context, boolean autoShowPermissionDialog){
        Intent intent = new Intent(context, CurrentLocationActivity.class);
        Bundle bundle = new Bundle();
        bundle.putBoolean(CurrentLocationActivity.KEY_AUTO_SHOW_LOCATION_PERMISSION, autoShowPermissionDialog);
        intent.putExtras(bundle);
        return intent;
    }
    @Override
    public void onLocationChanged(Location location) {
        hideProgressDialog();
        // we need just one location fix
        stopLocationUpdates();
        presenter.sendToSliderActivity(location);
    }



    @Override
    public void onLocationAccessStart(boolean isFirstLocationFix) {
        hideLocationAccessNote();
        if(isFirstLocationFix){
            showProgressDialog();
        }
    }

    @Override
    public void onLocationPermissionGranted() {
        // do nothing
    }

    protected void navigateToAddLocationScreen() {
        Intent intent = new Intent(this, ForceAddLocationActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onLocationPermissionDenied() {
        showLocationAccessNote();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.current_location_layout);
        presenter = new CurrentLocationActivityPresenter(this);
        binding.setPresenter(presenter);
    }

    @Override
    protected void onPause() {
        stopLocationUpdates();
        super.onPause();
    }

    protected void showLocationAccessNote(){
        binding.accessNoteContainer.setVisibility(View.VISIBLE);
    }
    protected void hideLocationAccessNote(){
        binding.accessNoteContainer.setVisibility(View.GONE);
    }

    @Override
    protected BaseActivityPresenter getPresenter() {
        return presenter;
    }

    protected boolean isLocationPermissionGranted(){
        return super.isLocationPermissionGranted();
    }
    public void requestForLocationPermission() {
        super.requestLocationPermission();
    }

    protected void navigateToSliderScreen(Intent intent) {
        startActivity(intent);
        finish();
    }
}
