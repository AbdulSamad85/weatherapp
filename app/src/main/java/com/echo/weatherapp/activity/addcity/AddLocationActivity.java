package com.echo.weatherapp.activity.addcity;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import com.echo.weatherapp.R;
import com.echo.weatherapp.activity.currentlocation.CurrentLocationActivity;
import com.echo.weatherapp.activity.slider.SliderActivity;
import com.echo.weatherapp.base.BaseActivity;
import com.echo.weatherapp.base.BaseActivityPresenter;
import com.echo.weatherapp.databinding.LayoutActivityAddCityBinding;
import com.echo.weatherapp.utility.DataManager;
import com.echo.weatherapp.utility.DialogUtils;
import com.google.android.gms.maps.model.LatLng;


/**
 * Created by abdulsamad on 03/09/2016.
 */

public class AddLocationActivity extends BaseActivity {

    protected AddLocationActivityPresenter presenter;
    protected LayoutActivityAddCityBinding binding;
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.layout_activity_add_city);
        presenter = new AddLocationActivityPresenter(this);
        binding.setPresenter(presenter);
        presenter.manageAddCurrentLocationVisibility();
        presenter.populateSavedLocations();
        presenter.initGoogleApiClient();
    }

    @Override
    protected BaseActivityPresenter getPresenter() {
        return presenter;
    }

    protected void showToast(String message){
        DialogUtils.showToast(this, message);
    }

    protected void finishActivity(){
        finish();
    }

    public DataManager getDataManager(){
        return dataManager;
    }

    protected void showMessage(final String message){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                DialogUtils.showToast(AddLocationActivity.this, message);
            }
        });
    }


    protected void navigateToSliderScreen() {
        navigateToSliderScreen(null);
    }

    protected void navigateToSliderScreen(Bundle bundle) {
        Intent intent = new Intent(this, SliderActivity.class);
        if(bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        finishActivity();
    }

    protected void navigateToCurrentLocationScreen() {
        Intent intent = CurrentLocationActivity.create(this, true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finishActivity();
    }

    protected void showAddCurrentLocationView(){
        binding.addCurrentLocation.setVisibility(View.VISIBLE);
    }

    protected void hideAddCurrentLocationView(){
        binding.addCurrentLocation.setVisibility(View.GONE);
    }

    protected void setResultToCaller(String name, LatLng latLng) {
        Bundle bundle = new Bundle();
        bundle.putString(AddLocationActivityPresenter.KEY_LOCATION_NAME, name);
        bundle.putParcelable(AddLocationActivityPresenter.KEY_LAT_LNG, latLng);
        Intent intent = new Intent();
        intent.putExtras(bundle);
        setResult(Activity.RESULT_OK, intent);
        finishActivity();
    }
}
