package com.echo.weatherapp.activity.editcity;

import com.echo.weatherapp.base.BaseActivityPresenter;

/**
 * Created by abdulsamad on 19/03/2018.
 */

public class EditLocationActivityPresenter extends BaseActivityPresenter{
    private EditLocationActivity view;

    public EditLocationActivityPresenter(EditLocationActivity view) {
        this.view = view;
    }

    @Override
    protected void onDestroyView() {
        view = null;
    }
}
