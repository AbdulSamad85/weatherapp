package com.echo.weatherapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.zetterstrom.com.forecast.models.DataPoint;
import android.zetterstrom.com.forecast.models.Forecast;

import com.echo.weatherapp.R;
import com.echo.weatherapp.interfaces.ILocationRemovalListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;


/**
 * Created by abdulsamad on 12/20/17.
 */

public class SavedLocationListAdapter extends RecyclerView.Adapter<SavedLocationListAdapter.MyViewHolder> {

    private List<Forecast> dataList;
    private Context context;
    private ILocationRemovalListener locationRemovalListener;
    /**
     * View holder class
     */
    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        ImageView remove;
        View rowView;

        MyViewHolder(View view) {
            super(view);
            rowView = view;
            title = view.findViewById(R.id.title);
            remove = view.findViewById(R.id.remove);
        }
    }

    public SavedLocationListAdapter(Context context, ILocationRemovalListener listener, List<Forecast> dataList) {
        this.dataList = dataList;
        this.context = context;
        this.locationRemovalListener = listener;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Forecast item = dataList.get(position);
        holder.title.setText(item.getLocation());
        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRemoveItemClicked(v);
            }
        });
        holder.remove.setTag(position);
        holder.rowView.setTag(item);

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context)
                .inflate(R.layout.layout_item_saved_location, parent, false);
        return new MyViewHolder(v);
    }

    private void onRemoveItemClicked(View v) {
        if(locationRemovalListener != null) {
            int position =(int)v.getTag();
            Forecast forecast = dataList.get(position);
            locationRemovalListener.onLocationRemoved(forecast);
            dataList.remove(position);
            notifyDataSetChanged();
        }
    }

}