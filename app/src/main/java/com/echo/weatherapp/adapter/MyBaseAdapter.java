package com.echo.weatherapp.adapter;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.widget.BaseAdapter;

import java.util.List;


public abstract class MyBaseAdapter extends BaseAdapter {

    protected final String TAG = this.getClass().getName();

    protected Context mContext = null;

    public List<Object> mAllData = null;

    protected LayoutInflater mInflater = null;

//	 protected String mCategory = null;

    protected Handler mScreenHandler = null;

//	protected AQuery aQuery = null;

    public MyBaseAdapter(Context context, int resource, List<Object> objects) {
        initData(context, objects);
    }

    public void initData(final Context mContext, final List<Object> mDataList) {
        this.mContext = mContext;
        this.mAllData = mDataList;
        if (mContext != null) {
            this.mInflater = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
    }

    public Context getContext() {
        return this.mContext;
    }

    public void setContext(final Context mContext) {
        this.mContext = mContext;
    }

    public List<Object> getData() {
        return this.mAllData;
    }

    public void setData(final List<Object> mDataList) {
        this.mAllData = mDataList;
    }

    public LayoutInflater getInflater() {
        return this.mInflater;
    }

    public void setInflater(final LayoutInflater mInflater) {
        this.mInflater = mInflater;
    }


    public String getTAG() {
        return this.TAG;
    }

    public void appendData(final List<Object> mDataList) {
        if (mDataList != null && !mDataList.isEmpty()) {
            if (mAllData == null) {
                mAllData = mDataList;
            } else {
                mAllData.addAll(mDataList);
            }
        }
        notifyDataSetChanged();
    }

    public Handler getScreenHandler() {
        return this.mScreenHandler;
    }

    public void setScreenHandler(final Handler mScreenHandler) {
        this.mScreenHandler = mScreenHandler;
    }

    public void setHanlder(final Handler handler) {
        this.setScreenHandler(this.mScreenHandler);
    }

    @Override
    public int getCount() {

        if (this.mAllData != null) {
            return this.mAllData.size();
        }
        return 0;
    }

    @Override
    public Object getItem(final int index) {
        if (this.mAllData != null) {
            return this.mAllData.get(index);
        }
        return null;
    }

    @Override
    public long getItemId(final int index) {

        return 0;
    }


    public void removeAllItems() {
        if (mAllData != null) {
            this.mAllData.clear();
            notifyDataSetChanged();
        }
    }

}
