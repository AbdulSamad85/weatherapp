package com.echo.weatherapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.zetterstrom.com.forecast.models.DataPoint;

import com.echo.weatherapp.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;


/**
 * Created by abdulsamad on 12/20/17.
 */

public class DailyForecastListAdapter extends RecyclerView.Adapter<DailyForecastListAdapter.MyViewHolder> {

    private List<DataPoint> dataList;
    private Context context;
    private DateFormat sdf;
    /**
     * View holder class
     */
    class MyViewHolder extends RecyclerView.ViewHolder {
//        TODO use databinding here
        TextView tvDay;
        TextView tvHotTemperature;
        View rowView;
        TextView tvColdTemperature;
        ImageView ivWeatherSymbol;
        MyViewHolder(View view) {
            super(view);
            rowView = view;
            tvDay = view.findViewById(R.id.tv_day);
            tvHotTemperature = view.findViewById(R.id.tv_hotTemperatureValue);
            tvColdTemperature = view.findViewById(R.id.tv_coldTemperatureValue);
             ivWeatherSymbol = view.findViewById(R.id.iv_weatherTypeSymbol);
        }
    }

    public DailyForecastListAdapter(Context context, List<DataPoint> dataList) {
        this.dataList = dataList;
        this.context = context;
        sdf = new SimpleDateFormat("EEEE", Locale.US);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        DataPoint item = dataList.get(position);
        holder.rowView.setTag(item);
        loadImage(holder, item);
        holder.tvDay.setText(sdf.format(item.getTime()));
        holder.tvHotTemperature.setText(item.getTemperatureMax() + "");
        holder.tvColdTemperature.setText(item.getTemperatureMin() + "");

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context)
                .inflate(R.layout.layout_item_daily_temperature, parent, false);
        return new MyViewHolder(v);
    }


    protected void loadImage(MyViewHolder holder, DataPoint item) {
        try {
            // incase some new symbol comes up
            int id =  context.getResources().getIdentifier(item.getIcon().getText().replaceAll("-","_").toLowerCase(), "drawable",
                    context.getPackageName());
            holder.ivWeatherSymbol.setImageResource(id);
        } catch (Exception ex){
//            TODO add crashlytics
        }
    }
}