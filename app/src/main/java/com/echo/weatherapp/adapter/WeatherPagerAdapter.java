package com.echo.weatherapp.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;
import android.zetterstrom.com.forecast.models.Forecast;

import com.echo.weatherapp.fragment.FragmentWeatherPage;

import java.util.ArrayList;

/**
 * Created by abdulsamad on 28/08/2016.
 */

public class WeatherPagerAdapter extends FragmentStatePagerAdapter {
    private ArrayList<Forecast> forecastList =  new ArrayList<>();
    ArrayList<String> titles = new ArrayList<>();


    public WeatherPagerAdapter(FragmentManager fragmentManager, ArrayList<String> titles, ArrayList<Forecast> forecastList) {
        super(fragmentManager);
        this.titles = titles;
        this.forecastList = forecastList;
    }

    // Returns total number of pages
    @Override
    public int getCount() {
        return forecastList.size();
    }

    // Returns the fragment to display for that page
    @Override
    public Fragment getItem(int position) {
        FragmentWeatherPage fragmentWeatherPage = FragmentWeatherPage.newInstance(forecastList.get(position), titles.get(position));
           return fragmentWeatherPage;
    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {
        return titles.get(position);
    }

    @Override
    public int getItemPosition(Object item) {
//        FragmentWeatherPage fragment = (FragmentWeatherPage) item;
//        String title = fragment.getTitle();
//        int position = titles.indexOf(title);
//
//        if (position >= 0) {
//            return position;
//        } else {
            return POSITION_NONE;
//        }
    }

    // Delete a page at a `position`
    public void deletePage(int position) {
        // Remove the corresponding item in the data set
        forecastList.remove(position);
        // Notify the adapter that the data set is changed
        notifyDataSetChanged();
    }



    // Unregister when the item is inactive
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
    }

    // Returns the fragment for the position (if instantiated)

    public ArrayList<String> getTitles() {
        return titles;
    }

    public ArrayList<Forecast> getForecastList() {
        return forecastList;
    }

}
