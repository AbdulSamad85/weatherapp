package com.echo.weatherapp.interfaces;

import android.zetterstrom.com.forecast.models.Forecast;

import java.util.List;

/**
 * Created by abdulsamad on 03/09/2016.
 */

public interface GetSavedLocationsCallback {
    void onLocationsRetrieved(List<Forecast> forecasts);
}
