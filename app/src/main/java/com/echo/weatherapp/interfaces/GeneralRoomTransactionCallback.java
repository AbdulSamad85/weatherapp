package com.echo.weatherapp.interfaces;

import android.zetterstrom.com.forecast.models.Forecast;

/**
 * Created by abdulsamad on 03/09/2016.
 */

public interface GeneralRoomTransactionCallback {
    void onResult(Object object);
}
