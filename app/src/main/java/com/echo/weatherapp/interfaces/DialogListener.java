package com.echo.weatherapp.interfaces;

/**
 * Created by AbdulSamad on 19/05/2015.
 */
public interface DialogListener {

    void onPositiveButtonClicked(String input);

    void onNegativeButtonClicked();
}
