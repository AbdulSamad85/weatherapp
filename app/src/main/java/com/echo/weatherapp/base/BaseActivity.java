package com.echo.weatherapp.base;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.zetterstrom.com.forecast.helper.DatabaseHelper;

import com.echo.weatherapp.dialog.CustomProgressDialog;
import com.echo.weatherapp.utility.DataManager;
import com.echo.weatherapp.utility.SharedPrefsHelper;
import com.echo.weatherapp.utility.Constants;

/**
 * Created by abdulsamad on 04/10/2016.
 */

public abstract class BaseActivity extends AppCompatActivity {

    protected CustomProgressDialog mCustomProgressDialog;
    protected DataManager dataManager;
    protected DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sharedPreferences = (this).getSharedPreferences(
                Constants.PREF_NAME, Context.MODE_PRIVATE);
        SharedPrefsHelper sharedPrefsHelper = new SharedPrefsHelper(sharedPreferences);
        ;
        dataManager = new DataManager(this , sharedPrefsHelper);
    }

    public void showProgressDialog() {
        if (mCustomProgressDialog != null && mCustomProgressDialog.isShowing()) {
            hideProgressDialog();
        }
        mCustomProgressDialog = CustomProgressDialog.show(this);
    }

    public void hideProgressDialog() {
        if (mCustomProgressDialog != null && mCustomProgressDialog.isShowing())
            mCustomProgressDialog.dismiss();
    }

    public DataManager getDataManager() {
        return dataManager;
    }

    public DatabaseHelper getDatabaseHelper() {
        return databaseHelper;
    }

    protected abstract BaseActivityPresenter getPresenter();

    @Override
    protected void onDestroy() {
        if(getPresenter() !=null){
            getPresenter().onDestroyView();
        }
        super.onDestroy();
    }
}
