package com.echo.weatherapp.base;

/**
 * Created by zaidbintariq on 16/12/2016.
 */

public  abstract class BaseFragmentPresenter {
    protected  abstract void onDestroyView();
}
