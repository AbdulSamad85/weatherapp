package com.echo.weatherapp.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;

import com.echo.weatherapp.activity.slider.SliderActivity;
import com.echo.weatherapp.dialog.CustomProgressDialog;


/**
 * Created by AbdulSamad on 14/10/2016.
 */

public abstract class BaseFragment extends Fragment {
    protected abstract BaseFragmentPresenter getPresenter();
    private CustomProgressDialog mCustomProgressDialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public synchronized void showProgressDialog() {
        if (mCustomProgressDialog != null && mCustomProgressDialog.isShowing()) {
            mCustomProgressDialog.dismiss();
        }
        mCustomProgressDialog = CustomProgressDialog.show(getActivity());
    }


    public synchronized void hideProgressDialog() {
        try {
            if (mCustomProgressDialog != null && mCustomProgressDialog.isShowing())
                mCustomProgressDialog.dismiss();

        } catch (final IllegalArgumentException e) {
            e.printStackTrace();
        }

    }

    public SliderActivity getDrawerActivity(){
        if(getActivity() instanceof SliderActivity){
            return ((SliderActivity)getActivity());
        } else {
            return null;
        }

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDestroyView() {
        View view = getView();
        if (view != null) {
            ViewGroup parentViewGroup = (ViewGroup) view.getParent();
            if (parentViewGroup != null) {
                parentViewGroup.removeAllViews();
            }
        }

        if (getPresenter() != null)  {
            getPresenter().onDestroyView();
        }
        super.onDestroyView();
    }

}
