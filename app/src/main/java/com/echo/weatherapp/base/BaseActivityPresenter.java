package com.echo.weatherapp.base;

/**
 * Created by zaidbintariq on 04/10/2016.
 */

public abstract class BaseActivityPresenter {
    protected abstract void onDestroyView();

}