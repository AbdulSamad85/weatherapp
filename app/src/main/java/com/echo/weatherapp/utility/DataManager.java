package com.echo.weatherapp.utility;

import android.content.Context;
import android.zetterstrom.com.forecast.helper.DatabaseHelper;
import android.zetterstrom.com.forecast.models.Forecast;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by abdulsamad on 18/03/2018.
 */

public class DataManager {

    private Context context;
    private SharedPrefsHelper sharedPrefsHelper;

    public DataManager( Context context,SharedPrefsHelper sharedPrefsHelper) {
        this.context = context;
        this.sharedPrefsHelper = sharedPrefsHelper;

    }

    public  void saveLocationForecastDownloadTime(String key, long milliseconds) {
        sharedPrefsHelper.put(key, milliseconds);
    }

    public  void removeLocationForecastDownloadTime(String key) {
        sharedPrefsHelper.deleteSavedData(key);
    }

    public  boolean shouldDownloadForecast(String locationName) {
        long downloadTime = sharedPrefsHelper.get(locationName, -1l);
        if (downloadTime == -1) {
            return true;
        }
        return System.currentTimeMillis() - downloadTime > TimeUnit.HOURS.toMillis(12);
    }

    public  Forecast getSavedForecast(String locationName){
        return DatabaseHelper.getInstance(context).ForecastDao().findByLocation(locationName);
    }



    public List<Forecast> getSavedForecasts(){
        return DatabaseHelper.getInstance(context).ForecastDao().getAll();
    }

    public void saveForecast(Forecast forecast){
        DatabaseHelper.getInstance(context).ForecastDao().insert(forecast);
    }

    public void deleteForecast(Forecast forecast){
        DatabaseHelper.getInstance(context).ForecastDao().delete(forecast);
    }

    public void deleteAllForecast(List<Forecast> forecasts){
        for(Forecast forecast: forecasts) {
            DatabaseHelper.getInstance(context).ForecastDao().delete(forecast);
        }
    }
}
